<?php

include "conn.php";

/* Database connection end */


// storing  request (ie, get/post) global array to a variable
$requestData = $_REQUEST;


$columns = array(
// datatable column index  => database column name
    0 => 'nombres',
    1 => 'cedula',
   // 2 => 'edad'
    /*3 => 'edad',
    4 => 'genero',
    5 => 'fecha_cirugia',
    6 => 'diagnostico',
    7 => 'codigo_cie',
    8 => 'cirujano',
   */// 9 => 'anestesiologo'
);


// getting total number records without any search
$sql = "SELECT nombres, cedula";
$sql .= " FROM paciente WHERE paciente_esta = 'Hospitalizado'";
$query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


if (!empty($requestData['search']['value'])) {
    // if there is a search parameter
    $sql = "SELECT id_pacinte, cedula, nombres, fecha_ingreso, nombre_departamento, piso, condicion, nu_cama, hora, paciente_esta, fecha_egreso, departamento_trasl";
    $sql .= " FROM paciente";
    $sql .= " WHERE nombres LIKE '%" . $requestData['search']['value'] . "%' ";    // $requestData['search']['value'] contains search parameter
 $sql .= " OR cedula LIKE '" . $requestData['search']['value'] . "%' ";
    $sql .= " OR fecha_ingreso LIKE '" . $requestData['search']['value'] . "%' ";
    $sql .= " OR nombre_departamento LIKE '" . $requestData['search']['value'] . "%' ";
    $sql .= " OR condicion LIKE '" . $requestData['search']['value'] . "%' ";
    $sql .= " OR fecha_egreso LIKE '" . $requestData['search']['value'] . "%' ";
   $sql .= " OR nu_cama LIKE '" . $requestData['search']['value'] . "%' ";
  $sql.=" OR piso LIKE '".$requestData['search']['value']."%' ";
  $sql.=" OR paciente_esta LIKE '".$requestData['search']['value']."%' ";
    $query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
    $totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

    $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
    $query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO"); // again run query with limit
} else {

    $sql = "SELECT id_pacinte, cedula, nombres, fecha_ingreso, nombre_departamento, piso, condicion, nu_cama, hora, paciente_esta, fecha_egreso, departamento_trasl";
    $sql .= " FROM paciente WHERE paciente_esta = 'Hospitalizado'";
   $sql .= " ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . "   ";
    //echo $sql; die();
    $query = mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
}
//echo $sql; die();
$data = array();
while ($row = mysqli_fetch_array($query)) {  // preparing an array
    $nestedData = array();

    //$nestedData[] = $row["id_hdia"];
      $nestedData[] = $row["cedula"];
      $nestedData[] = $row["nombres"];
      $nestedData[] = $row["fecha_ingreso"];
      $nestedData[] = $row["nombre_departamento"];
      $nestedData[] = $row["piso"];
      $nestedData[] = $row["condicion"];
	    $nestedData[] = $row["nu_cama"];
	    $nestedData[] = $row["hora"];
	    $nestedData[] = $row["paciente_esta"];
	    $nestedData[] = $row["fecha_egreso"];
    $nestedData[] = $row["departamento_trasl"];
      $nestedData[] = '<td><center>
        <a href="../profile.php?nik=' . $row['cedula'] . '"  data-toggle="tooltip" title="Revisar datos" class="btn btn-info">Datos </a>

				     </center></td>';

    $data[] = $nestedData;
}



$json_data = array(
    "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    "recordsTotal" => intval($totalData), // total number of records
    "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
    "data" => $data   // total data array
);

echo json_encode($json_data);  // send data as json format
?>
