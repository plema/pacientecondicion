<?php
include("conn.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Datos de Paciente</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-datepicker.css" rel="stylesheet">
        <link href="css/style_nav.css" rel="stylesheet">
        <style>
            .content {
                margin-top: 80px;
            }
        </style>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
          <?php
// escapar, además de eliminar todo lo que podría ser código (html / javascript-)
                $nik = mysqli_real_escape_string($conn, (strip_tags($_GET["nik"], ENT_QUOTES)));
              $sql = mysqli_query($conn, "SELECT * FROM paciente WHERE cedula='$nik'");
//print_r($sql) ;
                if (mysqli_num_rows($sql) == 0) {
                    header("Location: admin.php");
                } else {
                    $row = mysqli_fetch_assoc($sql);
                }
                if (isset($_POST['save'])) {
                  /* $paciente_esta =$_POST["paciente_esta"];
                   $fecha_egreso = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_egreso"], ENT_QUOTES))); //Escanpando caracteres
                   $cedula = mysqli_real_escape_string($conn, (strip_tags($_POST["cedula"], ENT_QUOTES))); //Escanpando caracteres
                   $nombres = mysqli_real_escape_string($conn, (strip_tags($_POST["nombres"], ENT_QUOTES))); //Escanpando caracteres
                   $nombre_departamento = mysqli_real_escape_string($conn, (strip_tags($_POST["nombre_departamento"], ENT_QUOTES))); //Escanpando caracteres
                  $fecha_ingreso = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_ingreso"], ENT_QUOTES))); //Escanpando caracteres

                  $hora = mysqli_real_escape_string($conn, (strip_tags($_POST["hora"], ENT_QUOTES))); //Escanpando caracteres

                  $piso = mysqli_real_escape_string($conn, (strip_tags($_POST["piso"], ENT_QUOTES))); //Escanpando caracteres
                   $nu_cama = mysqli_real_escape_string($conn, (strip_tags($_POST["nu_cama"], ENT_QUOTES))); //Escanpando caracteres
                  *///$condicion = mysqli_real_escape_string($conn, (strip_tags($_POST["condicion"], ENT_QUOTES))); //Escanpando caracteres
                  $fecha_traslado = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_traslado"], ENT_QUOTES))); //Escanpando caracteres
              $hora_traslado = mysqli_real_escape_string($conn, (strip_tags($_POST["hora_traslado"], ENT_QUOTES))); //Escanpando caracteres
                  $departamento_trasl = mysqli_real_escape_string($conn, (strip_tags($_POST["departamento_trasl"], ENT_QUOTES))); //Escanpando caracteres
                  $numero_cama_tras = mysqli_real_escape_string($conn, (strip_tags($_POST["numero_cama_tras"], ENT_QUOTES))); //Escanpando caracteres
                  $piso_traslado = mysqli_real_escape_string($conn, (strip_tags($_POST["piso_traslado"], ENT_QUOTES))); //Escanpando caracteres


$update = mysqli_query($conn, " UPDATE paciente SET fecha_traslado='$fecha_traslado', hora_traslado='$hora_traslado', departamento_trasl='$departamento_trasl', numero_cama_tras='$numero_cama_tras', piso_traslado='$piso_traslado' WHERE cedula='$nik'") or die(mysqli_error());
                    if ($update) {
                        header("Location: edit1.php?nik=" . $nik . "&pesan=sukses");
                    } else {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
                    }
                }

                if (isset($_GET['pesan']) == 'sukses') {
                    echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                }
                ?>
                <div class="content">
                <center> <img src="images/agenda1.png"></center>
                        <form class="form-horizontal" action="" method="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nombres y Apellidos</label>
                        <div class="col-sm-4">
                            <input type="text" name="nombres" value="<?php echo $row ['nombres']; ?>" class="form-control" placeholder="Nombres y Apellidos" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cedula</label>
                        <div class="col-sm-2">
                            <input type="text" name="cedula" value="<?php echo $row ['cedula']; ?>" class="form-control" placeholder="Cedula" required>
                        </div>
                    </div

                    ><div class="form-group">
                        <label class="col-sm-3 control-label">Departamento o Aréa </label>
                        <div class="col-sm-4">
                           <input  class="form-control"type="text" name="nombre_departamento" value="<?php echo $row['nombre_departamento']; ?>" placeholder="Dapartamento">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Fecha Ingreso</label>
                        <div class="col-sm-4">

                            <input type="text" name="fecha_ingreso" value="<?php echo $row ['fecha_ingreso']; ?>" class="form-control" placeholder="Fecha de Ingreso">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora</label>
                        <div class="col-sm-4">
                            <input type="text" name="hora" value="<?php echo $row ['hora']; ?>" class="form-control" placeholder="Hora Ingreso" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Piso</label>
                        <div class="col-sm-4">
                            <input  class="form-control"type="text" name="nacimiento" value="<?php echo $row['piso']; ?>" placeholder="Fecha de nacimiento">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Paciente (Alta/Hospitalizado)</label>
                        <div class="col-sm-3">

					<input  class="form-control"type="text" name="paciente_esta" value="<?php echo $row['paciente_esta']; ?>" placeholder="Estado del Paciente">
			</div>
			</div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Numero de cama</label>

                        <div class="col-sm-3">
                        <input type="text" name="nu_cama" value="<?php echo $row ['nu_cama']; ?>" class="form-control" placeholder="Numero de Cama" required>
                  </div>
                  </div>
                    <div class="form-group">

                        <label class="col-sm-3 control-label">Condición del Paciente</label>
<div class="col-sm-3">
                        <input  class="form-control" type="text" name="condicion" value="<?php echo $row['condicion']; ?>"placeholder="condicion">
                      </div>
                    </div>
                  <div class="form-group">
                        <label class="col-sm-3 control-label">Fecha Egreso</label>
                        <div class="col-sm-3">

                            <input type="date" name="fecha_egreso" value="<?php echo $row ['fecha_egreso']; ?>" class="form-control" placeholder="fecha egreso">
                        </div>
  </div>
                    <h3 class="alert-info"><strong>REGISTRO TRASLADO PACIENTE</strong></h3>
                            <div class="form-group">
                            <div class="row">
                              <!-- primera columna -->
                              <div class="col-md-4">
                          <p>Fecha de Traslado</p>			<input  class="form-control"type="date" name="fecha_traslado" value="<?php echo $row ['fecha_traslado']; ?>" placeholder="Fecha de Traslado">
                              </div>
                              <!-- segunda columna -->
                              <div class="col-md-4">
                        <p>Hora de Traslado</p>				<input type="text" id="timepicker" name="hora_traslado" class="timepicker form-control" value="<?php echo $row ['hora_traslado']; ?>" placeholder="Hora Traslado Paciente">
                              </div>
                              <!-- tercera columna -->
                              <div class="col-md-4">
                        <p>Departamento Traslado</p>
                        <select class="form-control" name="departamento_trasl"<?php echo $row ['departamento_trasl']; ?>>
                        <option>Elija Una Opción</option>
                        <option value="Traumatología"
                        <?php
                        if ($row["departamento_trasl"]=='Traumatología')
                        {
                          echo "selected";
                        }

                        ?>
                        >Traumatología</option>

                        <option value="Cirugía"

                        <?php
                        if ($row["departamento_trasl"]=='Cirugía')
                        {
                          echo "selected";
                        }

                        ?>
                        >Cirugía</option>

                        <option value="Maternidad"

                        <?php
                        if ($row["departamento_trasl"]=='Maternidad')
                        {
                          echo "selected";
                        }

                        ?>
                        >Maternidad</option>


                        <option value="Neonatología"

                        <?php
                        if ($row["departamento_trasl"]=='Neonatología')
                        {
                          echo "selected";
                        }

                        ?>
                        >Neonatología</option>

                        <option value="Medicina Interna"

                        <?php
                        if ($row["departamento_trasl"]=='Medicina Interna')
                        {
                          echo "selected";
                        }

                        ?>
                        >Medicina Interna</option>


                        <option value="Pediatría"

                        <?php
                        if ($row["departamento_trasl"]=='Pediatría')
                        {
                          echo "selected";
                        }

                        ?>
                        >Pediatría</option>


                        <option value="Centro Quirúrgico"

                        <?php
                        if ($row["departamento_trasl"]=='Centro Quirúrgico')
                        {
                          echo "selected";
                        }

                        ?>
                        >Centro Quirúrgico</option>

                        <option value="UCI"

                        <?php
                        if ($row["departamento_trasl"]=='UCI')
                        {
                          echo "selected";
                        }

                        ?>
                        >UCI</option>

                        <option value="Unidad de Quemados"

                        <?php
                        if ($row["departamento_trasl"]=='Unidad de Quemados')
                        {
                          echo "selected";
                        }

                        ?>
                        >Unidad de Quemados</option>


                        <option value="Neurocirugía"

                        <?php
                        if ($row["departamento_trasl"]=='Neurocirugía')
                        {
                          echo "selected";
                        }

                        ?>
                        >Neurocirugía</option>
                        </select>
                              </div>
                            </div>
                  </div>
                          <!-- segunda fila -->
                          <div class="form-group">
                            <div class="row">
                              <!-- primera columna -->
                              <div class="col-md-4">
                        <p>Piso</p>					  <select class="form-control" name="piso_traslado" value="<?php echo $row ['piso_traslado']; ?>">
                        <option>Elija Una Opción</option>
                        <option value="Piso 1"
                          <?php
                            if ($row["piso_traslado"]=='Piso 1')
                        {
                          echo "selected";
                        }

                          ?>
                        >Piso 1</option>

                        <option value="Piso 2"

                        <?php
                        if ($row["piso_traslado"]=='Piso 2')
                        {
                          echo "selected";
                        }

                        ?>
                        >Piso 2</option>

                        <option value="Piso 3"

                        <?php
                        if ($row["piso_traslado"]=='Piso 3')
                        {
                          echo "selected";
                        }

                        ?>
                        >Piso 3</option>

                        <option value="Piso 4"

                        <?php
                        if ($row["piso_traslado"]=='Piso 4')
                        {
                          echo "selected";
                        }

                        ?>
                        >Piso 4</option>

                        <option value="Planta Baja"

                        <?php
                        if ($row["piso_traslado"]=='Planta Baja')
                        {
                          echo "selected";
                        }

                        ?>
                        >Planta Baja</option>

                        <option value="Bloque A (antiguo)"

                        <?php
                        if ($row["piso_traslado"]=='Bloque A (antiguo)')
                        {
                          echo "selected";
                        }

                        ?>
                        >Bloque A (antiguo)</option>
                        </select>
                              </div>
                              <!-- segunda columna -->
                              <div class="col-md-4">

                              </div>
                              <!-- tercera columna -->
                              <div class="col-md-4">
                              </div>
                                <span class="col-md-4">
                        <p>Número de Cama</p>
                               <input  class="form-control"type="text" name="numero_cama_tras" value="<?php echo $row ['numero_cama_tras']; ?>" placeholder="Numero de Cama Traslado">
                                </span></div>
                          </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                            <input type="submit" name="save" class="btn btn-sm btn-primary" value="Guardar datos">
                            <a href="vista/admin.php" class="btn btn-sm btn-danger">Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
                      
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script>
            $('.date').datepicker({ format: 'dd-mm-yyyy', })
        </script>
    </body>
</html>
