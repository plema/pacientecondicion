<?php
include("conn.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Datos de Paciente</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-datepicker.css" rel="stylesheet">
        <link href="css/style_nav.css" rel="stylesheet">
        <style>
            .content {
                margin-top: 80px;
            }
        </style>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>


                 <?php
// escapar, además de eliminar todo lo que podría ser código (html / javascript-)
                $nik = mysqli_real_escape_string($conn, (strip_tags($_GET["nik"], ENT_QUOTES)));
                $sql = mysqli_query($conn, "SELECT * FROM paciente WHERE id_pacinte='$nik'");

                if (mysqli_num_rows($sql) == 0) {
                    header("Location: admin.php");
                } else {
                    $row = mysqli_fetch_assoc($sql);
                }
                if (isset($_POST['save'])) {
                   $paciente_esta =$_POST["paciente_esta"];
                   $fecha_egreso = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_egreso"], ENT_QUOTES))); //Escanpando caracteres
                   $cedula = mysqli_real_escape_string($conn, (strip_tags($_POST["cedula"], ENT_QUOTES))); //Escanpando caracteres
                   $nombres = mysqli_real_escape_string($conn, (strip_tags($_POST["nombres"], ENT_QUOTES))); //Escanpando caracteres
                   $nombre_departamento = mysqli_real_escape_string($conn, (strip_tags($_POST["nombre_departamento"], ENT_QUOTES))); //Escanpando caracteres
                  $fecha_ingreso = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_ingreso"], ENT_QUOTES))); //Escanpando caracteres

                  $hora = mysqli_real_escape_string($conn, (strip_tags($_POST["hora"], ENT_QUOTES))); //Escanpando caracteres

                  $piso = mysqli_real_escape_string($conn, (strip_tags($_POST["piso"], ENT_QUOTES))); //Escanpando caracteres
                   $nu_cama = mysqli_real_escape_string($conn, (strip_tags($_POST["nu_cama"], ENT_QUOTES))); //Escanpando caracteres
                  $condicion = mysqli_real_escape_string($conn, (strip_tags($_POST["condicion"], ENT_QUOTES))); //Escanpando caracteres
                  $fecha_traslado = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_traslado"], ENT_QUOTES))); //Escanpando caracteres
                  /*  $instrumentista = mysqli_real_escape_string($conn, (strip_tags($_POST["instrumentista"], ENT_QUOTES))); //Escanpando caracteres
                    $circulante = mysqli_real_escape_string($conn, (strip_tags($_POST["circulante"], ENT_QUOTES))); //Escanpando caracteres
                    $recuperacion = mysqli_real_escape_string($conn, (strip_tags($_POST["recuperacion"], ENT_QUOTES))); //Escanpando caracteres
                    $observacion = mysqli_real_escape_string($conn, (strip_tags($_POST["observacion"], ENT_QUOTES))); //Escanpando caracteres
                    */

$update = mysqli_query($conn, " UPDATE paciente SET fecha_traslado='$fecha_traslado', nu_cama='$nu_cama', piso='$piso', cedula='$cedula', nombres='$nombres', nombre_departamento='$nombre_departamento', hora='$hora', fecha_ingreso='$fecha_ingreso', fecha_egreso='$fecha_egreso', paciente_esta='$paciente_esta', condicion='$condicion' WHERE id_pacinte='$nik'") or die(mysqli_error());
                    if ($update) {
                        header("Location: edit.php?nik=" . $nik . "&pesan=sukses");
                    } else {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
                    }
                }

                if (isset($_GET['pesan']) == 'sukses') {
                    echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                }
                ?>
               
                <form class="form-horizontal" action="" method="post">
           <div class="container">
            <div class="content"> 
              <h3 class="alert-info"><strong>Datos del Paciente y Egreso Paciente
              </strong></h3>
              <hr />
                
                <div class="row">
					<!-- primera columna -->
					<div class="col-md-4">
<p><strong>Nombres y Apellidos del paciente</strong></p>	<input type="text" name="nombres" value="<?php echo $row ['nombres']; ?>" class="form-control" placeholder="Nombres y Apellidos" required>
					</div>
					<!-- segunda columna -->
					<div class="col-md-4">
<p><strong>Cedula</strong></p>			<input type="text" name="cedula" value="<?php echo $row ['cedula']; ?>" class="form-control" placeholder="Cedula" required>
					</div>
					<!-- tercera columna -->
					<div class="form-group">
                        <label class="col-sm-3 control-label">Departamento o Aréa </label>
                        <div class="col-sm-4">
                            <select class="form-control" name="nombre_departamento"<?php echo $row ['nombre_departamento']; ?>>

                    					<option value="Traumatología"
                    					<?php
                    					if ($row["nombre_departamento"]=='Traumatología')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Traumatología</option>

                    					<option value="Cirugía"

                    					<?php
                    					if ($row["nombre_departamento"]=='Cirugía')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Cirugía</option>

                    					<option value="Maternidad"

                    					<?php
                    					if ($row["nombre_departamento"]=='Maternidad')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Maternidad</option>


                    					<option value="Neonatología"

                    					<?php
                    					if ($row["nombre_departamento"]=='Neonatología')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Neonatología</option>

                    					<option value="Medicina Interna"

                    					<?php
                    					if ($row["nombre_departamento"]=='Medicina Interna')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Medicina Interna</option>


                    					<option value="Pediatría"

                    					<?php
                    					if ($row["nombre_departamento"]=='Pediatría')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Pediatría</option>


                    					<option value="Centro Quirúrgico"

                    					<?php
                    					if ($row["nombre_departamento"]=='Centro Quirúrgico')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Centro Quirúrgico</option>

                    					<option value="UCI"

                    					<?php
                    					if ($row["nombre_departamento"]=='UCI')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>UCI</option>

                    					<option value="Unidad de Quemados"

                    					<?php
                    					if ($row["nombre_departamento"]=='Unidad de Quemados')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Unidad de Quemados</option>


                    					<option value="Neurocirugía"

                    					<?php
                    					if ($row["nombre_departamento"]=='Neurocirugía')
                    					{
                    						echo "selected";
                    					}

                    					?>
                    					>Neurocirugía</option>
                    				</select>

                        </div>
                    </div>
                <hr />
                <div class="row">
					<!-- primera columna -->
					<div class="col-md-4">
<p><strong>Fecha de Ingreso</strong></p>	 <input type="text" name="fecha_ingreso" value="<?php echo $row ['fecha_ingreso']; ?>" class="form-control" placeholder="Fecha de Ingreso">
                 </div>
					<!-- segunda columna -->
					<div class="col-md-4">
<p><strong>Hora Ingreso</strong></p>		<input type="text" name="hora" value="<?php echo $row ['hora']; ?>" class="form-control" placeholder="Hora Ingreso" required>
                        </div>
					<!-- tercera columna -->
					<div class="col-md-4">
					</div>
			      <span class="col-md-4">
<p><strong>Piso</strong></p>
						<select class="form-control" name="piso"<?php echo $row ['piso']; ?>>
                              <option value="Piso 1"
                                <?php
                                  if ($row["piso"]=='Piso 1')
                              {
                                echo "selected";
                              }

                                ?>
                              >Piso 1</option>

                              <option value="Piso 2"

                              <?php
                              if ($row["piso"]=='Piso 2')
                              {
                                echo "selected";
                              }

                              ?>
                              >Piso 2</option>

                              <option value="Piso 3"

                              <?php
                              if ($row["piso"]=='Piso 3')
                              {
                                echo "selected";
                              }

                              ?>
                              >Piso 3</option>

                              <option value="Piso 4"

                              <?php
                              if ($row["piso"]=='Piso 4')
                              {
                                echo "selected";
                              }

                              ?>
                              >Piso 4</option>

                              <option value="Planta Baja"

                              <?php
                              if ($row["piso"]=='Planta Baja')
                              {
                                echo "selected";
                              }

                              ?>
                              >Planta Baja</option>

                              <option value="Bloque A (antiguo)"

                              <?php
                              if ($row["piso"]=='Bloque A (antiguo)')
                              {
                                echo "selected";
                              }

                              ?>
                              >Bloque A (antiguo)</option>
                            </select>
                         </span></div>
			</div>
                <hr />
                <div class="row">
					<!-- primera columna -->
					<div class="col-md-4">
<p><strong>Número de cama</strong></p>
				   	<input type="text" name="nu_cama" value="<?php echo $row ['nu_cama']; ?>" class="form-control" placeholder="Numero de Cama" required>
                  	</div>
					<!-- segunda columna -->
					<div class="col-md-4">
<p><strong>Condición Paciente</strong></p>	

						<select class="form-control" name="condicion"<?php echo $row ['condicion']; ?>>
              						<option value="Mejor"
              						<?php
              						if ($row["condicion"]=='Mejor')
              							{
              							echo "selected";
              							}

              						?>
              						>Mejor</option>
              						<option value="Igual"

              						<?php
              						if ($row["condicion"]=='Igual')
              							{
              							echo "selected";
              							}

              						?>
              						>Igual</option>
                       	  <option value="Grave"

							  <?php
                              if ($row["condicion"]=='Grave')
                              	{
                                echo "selected";
                              	}
    
                              ?>
                          	>Grave</option>
                            
                            <option value="Fallecido"

							  <?php
                              if ($row["condicion"]=='Fallecido')
                              	{
                                echo "selected";
                              	}
    
                              ?>
                          	>Fallecido</option>

              		</select>
                        
                    </div>
					<!-- tercera columna -->
                    <hr />
                    <hr />
                    <hr />
                    <h3 class="alert-info"><strong>Registro Egreso de Paciente</strong></h3>
                    <div class="col-md-4">
<p><strong>Paciente (Alta/Hospitalizado)</strong></p>
				   <select class="form-control" name="paciente_esta"<?php echo $row ['paciente_esta']; ?>>
						<option value="Hospitalizado"
						<?php
						if ($row["paciente_esta"]=='Hospitalizado')
						{
							echo "selected";
						}

						?>
						>Hospitalizado</option>
						<option value="Alta"

						<?php
						if ($row["paciente_esta"]=='Alta')
						{
							echo "selected";
						}

						?>
						>Alta</option>

					</select>
                    </div>
                    <div class="col-md-4">
<p><strong>Fecha Alta</strong></p>
				    <input type="date" name="fecha_egreso" value="<?php echo $row ['fecha_egreso']; ?>" class="form-control" placeholder="fecha egreso">
					</div>
                    <br>
                    <br>
                    <br><br>
                    <br>
                    <hr />
                    <div class="form-group">
                        <label class="col-sm-3 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                            <input type="submit" name="save"  class="btn btn-sm btn-primary" value="Guardar datos" >
                            <a href="vista/admin.php" class="btn btn-sm btn-danger">Cancelar</a>
                            
                        </div>
                    </div>
                  </div>
                      
                </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script>
            $('.date').datepicker({
                format: 'dd-mm-yyyy',
            })
        </script>
    </body>
</html>
