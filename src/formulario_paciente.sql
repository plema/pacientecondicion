-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-07-2019 a las 14:59:27
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `formulario_paciente`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id_departamento` int(11) NOT NULL,
  `nombre_depar` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `nombre_depar`) VALUES
(1, 'Traumatología'),
(2, 'Cirugía'),
(5, 'Maternidad'),
(6, 'Neonatología'),
(7, 'Medicina Interna'),
(8, 'Pediatría'),
(9, 'Centro Quirúrgico'),
(10, 'UCI'),
(11, 'Unidad de Quemados'),
(12, 'Neurocirugía ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `id_pacinte` int(11) NOT NULL,
  `nombres` varchar(200) NOT NULL,
  `fecha_ingreso` varchar(100) NOT NULL,
  `nombre_departamento` varchar(50) NOT NULL,
  `cedula` varchar(50) NOT NULL,
  `piso` varchar(100) NOT NULL,
  `condicion` varchar(100) NOT NULL,
  `nu_cama` varchar(100) NOT NULL,
  `hora` varchar(100) NOT NULL,
  `paciente_esta` varchar(100) NOT NULL,
  `fecha_egreso` varchar(100) NOT NULL,
  `fecha_traslado` varchar(50) NOT NULL,
  `hora_traslado` varchar(50) NOT NULL,
  `departamento_trasl` varchar(100) NOT NULL,
  `numero_cama_tras` varchar(100) NOT NULL,
  `piso_traslado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id_pacinte`, `nombres`, `fecha_ingreso`, `nombre_departamento`, `cedula`, `piso`, `condicion`, `nu_cama`, `hora`, `paciente_esta`, `fecha_egreso`, `fecha_traslado`, `hora_traslado`, `departamento_trasl`, `numero_cama_tras`, `piso_traslado`) VALUES
(5, 'JESSICA PAMELA CARDENAS CON', '07/07/2019', 'Traumatología', '05012', 'Piso 1', 'Igual', '35', '12 : 26 pm', 'Hospitalizado', '', '', '11:30', 'Medicina Interna', '5', 'Bloque A (antiguo)'),
(6, 'LUISA ESPERANZA MOLInari', '08/07/2019', 'Traumatología', '0502145522', 'Piso 2', 'Igual', '25', '5 : 18 PM', 'Hospitalizado', '', '', '11:30', 'Medicina Interna', '2', 'Bloque A (antiguo)'),
(7, 'RN LOPEZ CAMPAÑ2', '07/07/2019', 'Traumatología', '0502170720', 'Piso 2', 'Igual', '14', '12 : 30 AM', 'Hospitalizado', '', '2019-07-08', '11:30', 'Maternidad', '2', 'Piso 3'),
(8, 'LUISA ESPERANZA MOLINA', '07/08/2019', 'Maternidad', '0502146578', 'Piso 3', 'Igual', '25', '9 : 52 AM', 'Hospitalizado', '', '', '', '', '', ''),
(9, 'PATRICIO LLANGA', '07/08/2019', 'Traumatología', '0501246215', 'Piso 4', 'Igual', '26', '1 : 25 PM', 'Hospitalizado', '', '', '11:30', 'Cirugía', '16', 'Piso 4'),
(10, 'IRMA VALLE VALLE', '07/08/2019', 'Traumatología', '1802002558', 'Piso 4', 'Mejor', '25', '2 : 50 PM', 'Hospitalizado', '', '2019-07-08', '14:55', 'Cirugía', '65', 'Piso 4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `privilegio` int(2) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `email`, `password`, `privilegio`, `fecha_registro`) VALUES
(5, 'Delectus fugit', 'uadmin', 'dyxisev@yahoo.com', 'Pa$$w0rd!', 2, '2016-10-06 06:30:53'),
(7, 'Vsadsad', 'asdad', 'qusy@gmail.com', 'Pa$$w0rd!', 2, '2016-10-06 06:34:30'),
(9, 'william chicaiza', 'willy83', 'william.chicaiza@gmail.com', 'hpgl2019w', 1, '2019-06-14 15:03:27'),
(10, 'paco lema', 'paco.lema', 'paco.lema@gmail.com', 'hpgl2019', 1, '2019-06-17 15:14:46'),
(12, 'lorena', 'lorena', 'lorena@gmail.com', 'hpgl2019', 2, '2019-07-08 14:29:45');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id_departamento`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id_pacinte`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id_departamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id_pacinte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
