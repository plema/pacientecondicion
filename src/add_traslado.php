<?php
include("conn.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cirug&iacute;a</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-datepicker.css" rel="stylesheet">
        <link href="css/style_nav.css" rel="stylesheet">
        <style>
            .content {
                margin-top: 80px;
            }
        </style>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="content">
                <h2>Datos del Paciente &raquo; Agregar datos</h2>
                <hr />

                <?php
              //  print_r($_POST); die();
              // escapar, además de eliminar todo lo que podría ser código (html / javascript-)
                              $nik = mysqli_real_escape_string($conn, (strip_tags($_GET["nik"], ENT_QUOTES)));
                              $sql = mysqli_query($conn, "SELECT * FROM paciente WHERE id_pacinte='$nik'");
                              if (mysqli_num_rows($sql) == 0) {
                                  header("Location: admin.php");
                              } else {
                                  $row = mysqli_fetch_assoc($sql);
                              }
                if (isset($_POST['save'])) {

                    $fecha_traslado = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_traslado"], ENT_QUOTES))); //Escanpando caracteres
                    $hora_traslado = mysqli_real_escape_string($conn, (strip_tags($_POST["hora_traslado"], ENT_QUOTES))); //Escanpando caracteres
                    $departamento_trasl = mysqli_real_escape_string($conn, (strip_tags($_POST["departamento_trasl"], ENT_QUOTES))); //Escanpando caracteres
                    $numero_cama_tras = mysqli_real_escape_string($conn, (strip_tags($_POST["numero_cama_tras"], ENT_QUOTES))); //Escanpando caracteres
                    $piso_traslado = mysqli_real_escape_string($conn, (strip_tags($_POST["piso_traslado"], ENT_QUOTES))); //Escanpando caracteres
                    //$id_pacinte = mysqli_real_escape_string($conn, (strip_tags($_POST["id_pacinte"], ENT_QUOTES))); //Escanpando caracteres


                    $update = mysqli_query($conn, " UPDATE paciente SET fecha_traslado='$fecha_traslado', hora_traslado='$hora_traslado', departamento_trasl='$departamento_trasl', numero_cama_tras='$numero_cama_tras', piso_traslado='$piso_traslado' WHERE id_pacinte='$nik'") or die(mysqli_error());
                                        if ($update) {
                                            header("Location: add_traslado.php?nik=" . $nik . "&pesan=sukses");
                                        } else {
                                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.</div>';
                                        }
                                    }

                                    if (isset($_GET['pesan']) == 'sukses') {
                                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                                    }
                ?>

                <script>
                    $('.date').datepicker({
                        format: 'dd-mm-yyyy',
                    })
                </script>
                </body>
                </html>

                <script type="text/javascript"> function controltag(e) {
                        tecla = (document.all) ? e.keyCode : e.which;
                        if (tecla == 8)
                            return true;
                        else if (tecla == 0 || tecla == 9)
                            return true;
                        // patron =/[0-9\s]/;// -> solo letras
                        patron = /[0-9\s]/;// -> solo numeros
                        te = String.fromCharCode(tecla);
                        return patron.test(te);
                    }
                </script>
                <script type="text/javascript">   //Se utiliza para que el campo de texto solo acepte letras
                    function soloLetras(e) {
                        key = e.keyCode || e.which;
                        tecla = String.fromCharCode(key).toString();
                        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
                        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

                        tecla_especial = false
                        for (var i in especiales) {
                            if (key == especiales[i]) {
                                tecla_especial = true;
                                break;
                            }
                        }

                        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                            alert('Tecla no aceptada');
                            return false;
                        }
                    }
                </script>
