<?php
include("conn.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
<link rel="stylesheet" href="tabla.css">

        <title>Datos de pacientes</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style_nav.css" rel="stylesheet">
        <style>
            .content {
                margin-top: 5px;
            }
        </style>


    </head>
    <body>

        <div class="container">
            <div class="content">
                <hr />
				<center> <img src="images/agenda1.png"></center>
                <?php
                // escaping, additionally removing everything that could be (html/javascript-) code
                $nik = mysqli_real_escape_string($conn, (strip_tags($_GET["nik"], ENT_QUOTES)));

                $sql = mysqli_query($conn, "SELECT * FROM paciente WHERE cedula='$nik'");
                if (mysqli_num_rows($sql) == 0) {
                    header("Location: index-usuario.php");
                } else {
                    $row = mysqli_fetch_assoc($sql);
                }

                if (isset($_GET['aksi']) == 'delete') {
                    $delete = mysqli_query($conn, "DELETE FROM pacientes WHERE cedula='$nik'");
                    if ($delete) {
                        echo '<div class="alert alert-danger alert-dismissable">><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data berhasil dihapus.</div>';
                    } else {
                        echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Data gagal dihapus.</div>';
                    }
                }
                ?>
<form method="POST" action="add_traslado.php" style="margin:5%; background-color:#C8E1F5; padding: 3%; ">
			<!-- primera fila -->

			<div class="form-group">
				<div class="row">
					<!-- primera columna -->
					<div class="col-md-4">
			<p>Nombres y Apellidos del paciente</p>			<input  class="form-control"type="text" value="<?php echo $row['nombres']; ?>"name="nombres" placeholder="Nombres">
					</div>
					<!-- segunda columna -->
					<div class="col-md-4">
<p>Cedula</p>			<input  class="form-control"type="text" name="apellidos" value="<?php echo $row['cedula']; ?>"placeholder="Cedula">
					</div>
					<!-- tercera columna -->
					<div class="col-md-4">
<p>Fecha de Ingreso</p>						<input  class="form-control"type="text" name="nacimiento" value="<?php echo $row['fecha_ingreso']; ?>" placeholder="Fecha de nacimiento">
					</div>
				</div>
			</div>
			<!-- segunda fila -->
			<div class="form-group">
				<div class="row">
					<!-- primera columna -->
					<div class="col-md-4">
<p>Piso</p>						<input  class="form-control"type="text" name="nacimiento" value="<?php echo $row['piso']; ?>" placeholder="Fecha de nacimiento">

					</div>
					<!-- segunda columna -->
					<div class="col-md-4">
<p>Condición del Paciente</p>						<input  class="form-control" type="text" name="condicion" value="<?php echo $row['condicion']; ?>"placeholder="condicion">
					</div>
					<!-- tercera columna -->
					<div class="col-md-4">
					</div>
			      <span class="col-md-4">
<p>Número de Cama</p>
				    <input  class="form-control"type="text" name="nu_cama" value="<?php echo $row['nu_cama']; ?>"placeholder="Numero de Cama">
		        </span></div>
			</div>
			<!-- tercera fila -->
			<div class="form-group">
				<div class="row">
					<!-- primera columna -->
					<div class="col-md-4">
<p>Hora Ingreso</p>
				    <input  class="form-control"type="text" name="hora" value="<?php echo $row['hora']; ?>" placeholder="Hora">
					</div>
					<!-- segunda columna -->
					<div class="col-md-4">
<p>Estado Paciente</p>						<input  class="form-control"type="text" name="paciente_esta" value="<?php echo $row['paciente_esta']; ?>" placeholder="Estado del Paciente">
					</div>
					<!-- tercera columna -->
					<div class="col-md-4">
<p>Departamento o Área</p>
				    <input  class="form-control"type="text" name="nombre_departamento" value="<?php echo $row['nombre_departamento']; ?>" placeholder="Dapartamento">
					</div>
                    <div class="col-md-4">
<p>Fecha Alta</p>
				    <input  class="form-control"type="text" name="fecha_egreso"                  
                    value="<?php echo $row['fecha_egreso']; ?>" placeholder="Fecha egreso">
					</div>
                    <br>
                    <br>
                    <br><br>
                    <br>
                    <br>
                   <div class="col-md-4">
					
					</div>
                    <div class="col-md-4">
                    </div>
                    <br>
                    <br>
                    <br>
                    <center style="color:#060708;font-size:120%;"><p><strong>DATOS TRASLADO</strong></p>
                    </center>
                    <br>
                    <div class="col-md-4">
<p>Fecha Traslado</p>
				    <input  class="form-control"type="text" name="fecha_traslado" value="<?php echo $row['fecha_traslado']; ?>" placeholder="Fecha Traslado">
					</div>
                    <div class="col-md-4">
<p>Hora Traslado</p>
				    <input  class="form-control"type="text" name="hora_traslado" value="<?php echo $row['hora_traslado']; ?>" placeholder="Hora Traslado">
					</div>
                    <div class="col-md-4">
<p>Departamento Traslado</p>
				    <input  class="form-control"type="text" name="departamento_trasl" value="<?php echo $row['departamento_trasl']; ?>" placeholder="Dapartamento Traslado">
					</div>
                    <div class="col-md-4">
<p>Número Cama Traslado</p>
				    <input  class="form-control"type="text" name="numero_cama_tras" value="<?php echo $row['numero_cama_tras']; ?>" placeholder="Número Cama Traslado">
					</div>
                    <div class="col-md-4">
<p>Piso Traslado</p>
				    <input  class="form-control"type="text" name="piso_traslado" value="<?php echo $row['piso_traslado']; ?>" placeholder="Piso Traslado">
					</div>
                    
				</div>
                	<br>
                    <br>
                    <br>
               		<center> <a href="vista/index-usuario.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Regresar</a></center>
			</div>
		</form>

                

            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
