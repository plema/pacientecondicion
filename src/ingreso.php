<?php
include("conn.php");
$mysqli = new mysqli('localhost', 'root', '', 'formulario_paciente');

?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
	<title>Formulario HGL :</title>
<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' /><link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts-->
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
<!--//fonts-->
</head>
<body>

<!--background-->
<center> <img src="images/agenda.png"></center>
    <div class="bg-agile">
	<div class="book-appointment">
				<form method="POST" action="add.php">

			<div id="resultados_ajax" class="gaps"></div>
			<div class="left-agileits-w3layouts same">

			<div class="gaps">
				<p>Nombres y Apellidos del paciente</p>
					<input type="text" name="nombres" placeholder="" onkeypress="return soloLetras(event);" onKeyUp="this.value = this.value.toUpperCase();"required/>
			</div>
				<div class="gaps">
				<p>Número de Cedula</p>
					<input type="text" name="cedula" placeholder="" maxlength="10" onkeypress="return controltag(event)"required/>
				</div>
				<div class="gaps">
				<p>Piso</p>
					<select class="form-control" name="piso">
						<option></option>
						<option value="Piso 1">Piso 1</option>
						<option value="Piso 2">Piso 2</option>
						<option value="Piso 3">Piso 3</option>
						<option value="Piso 4">Piso 4</option>
						<option value="Planta Baja">Planta Baja</option>
            <option value="Bloque A (antiguo)">Bloque A (antiguo)</option>
					</select>
			</div>
				<div class="gaps">
				<p>Número de cama</p>
						<input type="text" name="nu_cama" placeholder="" required >
				</div>
                <div class="gaps">
				<p>Paciente (Alta/Hospitalizado)</p>
					<select class="form-control" name="paciente_esta">
						<option></option>
						<option value="Hospitalizado">Hospitalizado</option>
						<option value="Alta">Alta</option>

					</select>
			</div>
			</div>
			<div class="right-agileinfo same">
			<div class="gaps">
				<p>Seleccionar fecha Ingreso</p>
						<input  id="fecha_ingreso" name="fecha_ingreso" type="date" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required>
			</div>
			<div class="gaps">
			  <p>Departamento o Aréa</p>
				<select class="form-control" name="nombre_departamento">
					<option></option>
				  <option value="Traumatología">Traumatología</option>
					<option value="Cirugía">Cirugía</option>
					<option value="Maternidad">Maternidad</option>
					<option value="Neonatología">Neonatología</option>
					<option value="Medicina Interna">Medicina Interna</option>
					 <option value="Pediatría">Pediatría</option>
					 <option value="Centro Quirúrgico">Centro Quirúrgico</option>
					 <option value="UCI">UCI</option>
					 <option value="Unidad de Quemados">Unidad de Quemados</option>
					 <option value="Neurocirugía">Neurocirugía</option>

</select>
	          </div>

			<div class="gaps">
				<p>Condición del Paciente</p>
					<select class="form-control" name="condicion" required>
						<option></option>
						<option value="Mejor">Mejor</option>
						<option value="Igual">Igual</option>
                        <option value="Grave">Grave</option>
                        <option value="Fallecido">Fallecido</option>
					</select>
			</div>

			<div class="gaps">
				<p>Hora</p>
					<input type="text" id="timepicker" name="hora" class="timepicker form-control" value="">
			</div>
					<div class="gaps"><p><input type="submit" name="add" value="Ingreso"></p></div>
					<div class="gaps"><p><input type="submit" name="admin" value="Cancelar" onClick="history.back()"></p> </div>
			</div>

			<div class="clear"></div>

		</form>

		</div>
   </div>
   <!--copyright-->
			<div class="copy w3ls">
		       <p>&copy; 2019. Formulario de registro hospitalizados en línea HGL. All Rights Reserved  </p>
	        </div>
		<!--//copyright-->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="js/wickedpicker.js"></script>
			<script type="text/javascript">
				$('.timepicker').wickedpicker({twentyFour: false});
			</script>
		<!-- Calendar -->
				<link rel="stylesheet" href="css/jquery-ui.css" />
				<script src="js/jquery-ui.js"></script>
				  <script>
						  $(function() {
							$( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
						  });

				  </script>
			<!-- //Calendar -->

<script type="text/javascript"> function controltag(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 8)
            return true;
        else if (tecla == 0 || tecla == 9)
            return true;
        // patron =/[0-9\s]/;// -> solo letras
        patron = /[0-9\s]/;// -> solo numeros
        te = String.fromCharCode(tecla);
        return patron.test(te);
    }
</script>
<script type="text/javascript">   //Se utiliza para que el campo de texto solo acepte letras
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            alert('Tecla no aceptada');
            return false;
        }
    }
</script>
</body>
</html>
