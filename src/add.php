<?php
include("conn.php");
?>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cirug&iacute;a</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-datepicker.css" rel="stylesheet">
        <link href="css/style_nav.css" rel="stylesheet">
        <style>
            .content {
                margin-top: 80px;
            }
        </style>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="content">
                <h2>Datos del Paciente &raquo; Agregar datos</h2>
                <hr />

                <?php
               // print_r($_POST); die();

                if (isset($_POST['add'])) {

                    $nombre_departamento = mysqli_real_escape_string($conn, (strip_tags($_POST["nombre_departamento"], ENT_QUOTES))); //Escanpando caracteres
                    $nombres = mysqli_real_escape_string($conn, (strip_tags($_POST["nombres"], ENT_QUOTES))); //Escanpando caracteres
                    $fecha_ingreso = mysqli_real_escape_string($conn, (strip_tags($_POST["fecha_ingreso"], ENT_QUOTES))); //Escanpando caracteres
                    $cedula = mysqli_real_escape_string($conn, (strip_tags($_POST["cedula"], ENT_QUOTES))); //Escanpando caracteres
                    $piso = mysqli_real_escape_string($conn, (strip_tags($_POST["piso"], ENT_QUOTES))); //Escanpando caracteres
                    $condicion = mysqli_real_escape_string($conn, (strip_tags($_POST["condicion"], ENT_QUOTES))); //Escanpando caracteres
                    $nu_cama = mysqli_real_escape_string($conn, (strip_tags($_POST["nu_cama"], ENT_QUOTES))); //Escanpando caracteres
                    $hora = mysqli_real_escape_string($conn, (strip_tags($_POST["hora"], ENT_QUOTES))); //Escanpando caracteres
                    $paciente_esta = mysqli_real_escape_string($conn, (strip_tags($_POST["paciente_esta"], ENT_QUOTES))); //Escanpando caracteres

 $insert = mysqli_query($conn, "INSERT INTO paciente(nombres, fecha_ingreso, nombre_departamento, cedula, piso, condicion, nu_cama, hora, paciente_esta)
VALUES ('$nombres', '$fecha_ingreso', '$nombre_departamento', '$cedula', '$piso', '$condicion', '$nu_cama', '$hora', '$paciente_esta')") or die(mysqli_error());
                    if ($insert) {


                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Bien hecho! Los datos han sido guardados con éxito.</div>';
                    } else {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. No se pudo guardar los datos !</div>';
                    }

//				}else{
//					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. código exite!</div>';
//				}
                }
                header('Location: vista/admin.php');
                ?>

                <script>
                    $('.date').datepicker({
                        format: 'dd-mm-yyyy',
                    })
                </script>
                </body>
                </html>

                <script type="text/javascript"> function controltag(e) {
                        tecla = (document.all) ? e.keyCode : e.which;
                        if (tecla == 8)
                            return true;
                        else if (tecla == 0 || tecla == 9)
                            return true;
                        // patron =/[0-9\s]/;// -> solo letras
                        patron = /[0-9\s]/;// -> solo numeros
                        te = String.fromCharCode(tecla);
                        return patron.test(te);
                    }
                </script>
                <script type="text/javascript">   //Se utiliza para que el campo de texto solo acepte letras
                    function soloLetras(e) {
                        key = e.keyCode || e.which;
                        tecla = String.fromCharCode(key).toString();
                        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
                        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

                        tecla_especial = false
                        for (var i in especiales) {
                            if (key == especiales[i]) {
                                tecla_especial = true;
                                break;
                            }
                        }

                        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                            alert('Tecla no aceptada');
                            return false;
                        }
                    }
                </script>
