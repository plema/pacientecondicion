﻿<?php include "../conn.php"; ?>
<?php include 'partials/head.php';?>
<?php
if (isset($_SESSION["usuario"])) {
    if ($_SESSION["usuario"]["privilegio"] == 1) {
        header("location:admin.php");
    }
} else {
    header("location:index.php");
}
?>
<?php include 'partials/menu.php';?>
<!DOCTYPE html>
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style_nav.css" rel="stylesheet">

<style>
    .content {
        margin-top: 80px;
    }
</style>
<html lang="en">


    <body>


        <div class="container">
            <div class="row">
                <div class="span12">
                  <div class="content">
                      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <?php
                        if (isset($_GET['aksi']) == 'delete') {
                            // escapando, además eliminando todo lo que podría ser código (html / javascript-)
                            $nik = mysqli_real_escape_string($conn, (strip_tags($_GET["nik"], ENT_QUOTES)));
                            $cek = mysqli_query($conn, "SELECT * FROM paciente WHERE cedula='$nik'");
                            if (mysqli_num_rows($cek) == 0) {
                              //  echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
                            } else {
                                $delete = mysqli_query($conn, "DELETE FROM paciente WHERE cedula='$nik'");
                                if ($delete) {
                                  //  echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
                                } else {
                                  //  echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.</div>';
                                }
                            }
                        }
                       // print_r($_COOKIE);

                        ?>
                    <div class="panel panel-default">
                            <div class="panel-heading">
                              <center  <h3 class="panel-title"><i class="icon-user"></i> Listado de pacientes</h3> </center>

                            </div>


                                  </div>
                                  <p>&nbsp;</p>
                                  <p>&nbsp;</p>


                                </form>
                      </div>
                    </div>
                    <div class="panel-body">
                      <table id="lookup" class="table table-bordered table-hover">
                          <thead bgcolor="#eeeeee" align="center">
                                <tr>

                                    <th>Cedula</th>
                                    <th>Nombres y Apellidos</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Departamento o Aréa</th>
                                    <th>Piso</th>
                                    <th>Condicón del Paciente</th>
                                    <th>Numero de cama</th>
                                    <th>Hora</th>
                                    <th>Paciente (Alta/Hospitalizado)</th>
                                    <th>Fecha Egreso</th>
                                    <th>Departamento Traslado</th>
<th style="text-align: center" width="170px">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
            <!--/.content-->
        </div>
        <!--/.span9-->
    </div>
</div>
<!--/.container-->

<!--/.wrapper--><br />
<div class="footer span-12">
    <div class="container">
        <center> <b class="copyright"><a> Sistemas Web</a> &copy; <?php echo date("Y") ?> Registro de Pacientes HGL </b></center>
    </div>
</div>
<?php include 'partials/footer.php';?>
<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="../datatables/jquery.dataTables.js"></script>
<script src="../datatables/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        var dataTable = $('#lookup').DataTable({

            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",

                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },

                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },

            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "../ajax-grid-data-usuario.php", // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".lookup-error").html("");
                    $("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos en el servidor</th></tr></tbody>');
                    $("#lookup_processing").css("display", "none");

                }
            }
        });
    });
</script>

</body>
