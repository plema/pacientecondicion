<?php

$db_host = "localhost";
$db_user = "root";
$db_pass = "";
$db_name = "formulario_paciente";

$conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
mysqli_set_charset($conn,"utf8");
if (mysqli_connect_errno()) {
    echo 'Error, no se pudo conectar a la base de datos: ' . mysqli_connect_error();
}
?>
